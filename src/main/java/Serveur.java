import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {

    public static void main(String[] args) {

        ServerSocket serverSocket;
        Socket clientSocket;

        try {
            // Écouter sur le port
            serverSocket = new ServerSocket(8080);

            // Attendre la connexion et l'accepter
            clientSocket = serverSocket.accept();
            System.out.printf("DEBUG: Connexion du client: %s\n", clientSocket.getInetAddress().getHostAddress());

            // Obtenir les flux d'entrée et de sortie
            PrintWriter socketOut = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader socketIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            // Afficher un message de bienvenue
            socketOut.printf("Bienvenue! Comment vous appelez-vous? \n");

            // Obtenir le nom
            String name = socketIn.readLine();
            System.out.println("DEBUG: Message du client: " + name);

            // Afficher une salutation
            socketOut.printf("Bonjour, %s!\n", name);

            // Fermer la connexion
            clientSocket.close();
            serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
